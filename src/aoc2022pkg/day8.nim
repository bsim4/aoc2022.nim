import arraymancer

import std / strutils

const testInput = """30373
25512
65332
33549
35390"""


type
  Dir = enum
    X, Y

proc checkTallest(treeHeights: Tensor, dir: Dir,currHeight, j, min, max: int) : bool = 
  result = true
  for x in min ..< max:
    let checkHeight = case dir
    of X: 
      treeHeights[x, j]
    of Y:
      treeHeights[j, x]
    
    if currHeight <= checkHeight:
      result = false
      break
 
 
proc makeTensor(fileText:string ) :  Tensor[int] = 
  var lines = fileText.strip().split("\n")
  let H = lines.len
  let W = lines[0].len
  result = newTensor[int]([H, W])
  for i, l in lines:
    for j, h in l:
      result[i, j] = parseInt($h)


proc countVisible(treeHeights: Tensor ) : int = 
  let dims = treeHeights.shape
  let H = dims[0]
  let W = dims[1]
  result = 2 * H + 2 * (W-2)
  
  for i in 1..<H-1:
    for j in 1..<W-1:
      let currHeight = treeHeights[i, j]
      if checkTallest(treeHeights, X, currHeight, j, i, 0):
        result += 1
      elif checkTallest(treeHeights, X, currHeight, j, i+1, H):
        result += 1
      elif checkTallest(treeHeights, Y, currHeight, i, j, 0):
        result += 1
      elif checkTallest(treeHeights, Y, currHeight, i, j+1, W):
        result += 1

iterator count(a, b: int): int =
  var i = a

  let cond = proc(i: int) : bool =
    if a <= b:
      return i <= b
    else:
      return i >= b

  while cond(i):
    yield i
    if a <= b:
      inc i
    else:
      dec i


proc scoreDir(treeHeights: Tensor, dir: Dir,currHeight, j, min, max: int) : int= 
  result = 0
  for x in count(min.int, max.int):

    let checkHeight = case dir
    of X: 
      treeHeights[x, j]
    of Y:
      treeHeights[j, x]
    
    result += 1
    if currHeight <= checkHeight:
      break

 
  
proc scoreVisible(treeHeights: Tensor) : int = 
  let dims = treeHeights.shape
  let H = dims[0]
  let W = dims[1]
  
  result = low(int)
  for i in 1..<H-1:
    for j in 1..<W-1:
      var currScore = 1
      let currHeight = treeHeights[i, j]
      
      currScore *= scoreDir(treeHeights, X, currHeight, j, i-1, 0)
      currScore *= scoreDir(treeHeights, X, currHeight, j, i+1, H-1)
      currScore *= scoreDir(treeHeights, Y, currHeight, i, j-1, 0)
      currScore *= scoreDir(treeHeights, Y, currHeight, i, j+1, W-1)

      if currScore > result:
        result = currScore



proc solve*() = 
  echo "Day 8 Solution"
  let fText = open("assets/day8.txt").readAll()
  let treeHeights = makeTensor(fText)
  # let treeHeights = makeTensor(testInput)
  echo "\tPart 1: ", countVisible(treeHeights)
  echo "\tPart 2: ", scoreVisible(treeHeights)
