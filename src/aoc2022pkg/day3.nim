import std/sequtils

func getPriority(c: char) : int =
  case c
  of 'A'..'Z': return ord(c) - ord('A') + 27
  of 'a'..'z': return ord(c) - ord('a') + 1
  else: raise new ValueError


proc getCommon(ss: openArray[string]) : char =
  for c in ss[0]:
    if ss[1..^1].allIt(c in it): return c
  raise new ValueError


proc getMisplaced(s: string) : char =
  let l = s.len div 2
  return getCommon([s[0..<l],s[l..^1] ])

proc part1() : int =
  let f = open("./assets/day3.txt")
  defer: f.close()

  for l in f.lines:
    let c = getMisplaced(l)
    result += getPriority(c)

proc part2() : int =
  let f = open("./assets/day3.txt")
  defer: f.close()

  var l_num = 0
  var curr_lines: array[3, string]
  for l in f.lines:
    curr_lines[l_num mod 3] = l
    l_num += 1
    if l_num  mod 3 == 0:
      let c = getCommon(curr_lines)
      result += getPriority(c)


proc solve*() = 
  echo "Day 3 Solution"
  echo "\tPart 1: ", part1() 
  echo "\tPart 2: ", part2() 
