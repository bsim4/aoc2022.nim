import std / strutils
import std / sequtils

type

  FSObj = ref object of RootObj
    name: string
    parent: Dir

  File = ref object of FSObj
    size: Natural

  Dir = ref object of FSObj
    children: seq[FSObj]
    rsize: Natural

  Op = enum
    CD, LS

proc getOp(s: string) : Op =
  case s
  of "ls": return LS
  of "cd": return CD
  else: raise new ValueError


var rootDir = Dir(name: "/", children: @[], parent: nil)

var currDir = rootDir;

const maxDirSize : Natural = 100000


proc calculateDirSize(startDir: Dir) : Natural = 
  for c in startDir.children:
    if c of File:
      result += c.File.size
    else:
      result += calculateDirSize(c.Dir)
  startDir.rsize = result


proc calculateTotalDeleteSize(startDir: Dir) : Natural =
  if startDir.rsize < maxDirSize:
    result += startDir.rsize
  for c in startDir.children:
    if c of Dir:
      result += calculateTotalDeleteSize(c.Dir)


proc solvePartOne() : int =
  let f = open("assets/day7.txt")
  defer: f.close()

  for l in f.lines():
    if l.startsWith("$"):
      case getOp(l[2..3])
      of LS: continue
      of CD:
        let dirName = l.replace("$ cd ", "")
        case dirName
        of "/": currDir = rootDir
        of "..": currDir = currDir.parent
        else: currDir = filter(currDir.children, proc(x: FSObj): bool = x of Dir and x.name == dirName)[0].Dir

    elif l.startsWith("dir"):
      let parts = l.split(" ")
      currDir.children.add Dir(name: parts[^1], parent: currDir) 
    else:
      let parts = l.split(" ")
      let size = parts[0].parseInt
      let name = parts[^1]
      currDir.children.add File(size: size, name: name, parent: currDir) 

  discard calculateDirSize(rootDir)

  result = calculateTotalDeleteSize(rootDir)

const totalDiskSpace : Natural = 70000000
const requiredFreeSpace: Natural = 30000000 

proc getMinSpace(startDir: Dir, requiredDeleteSpace: Natural, currentValidDir: var Dir) = 
  if startDir.rsize >= requiredDeleteSpace:
    if currentValidDir == nil:
      currentValidDir = startDir
    else:
      if startDir.rsize < currentValidDir.rsize:
        currentValidDir = startDir
  for c in startDir.children:
    if c of Dir:
      getMinSpace(c.Dir, requiredDeleteSpace, currentValidDir)

proc solvePartTwo() : Dir =
  let currentFreeSpace = totalDiskSpace - rootDir.rsize
  let requiredDeleteSpace = requiredFreeSpace - currentFreeSpace
  var minDir : Dir
  getMinSpace(rootDir, requiredDeleteSpace, minDir)
  result = minDir

proc `$`(d: Dir): string =
  result = "Dir(name: " & d.name & "rsize: " & $d.rsize & ")"

proc solve*() =
  echo "Day 7 Solution"
  echo "\tPart 1: ", solvePartOne()
  echo "\tPart 2: ", solvePartTwo()




