import std/strutils

type
  Hand = enum
    Rock, Paper, Scissor
  Outcome = enum
    Lose, Draw, Win

func parseInput(c: string) : Hand =
  case c
  of "A", "X": return Rock
  of "B", "Y": return Paper
  of "C", "Z": return Scissor
  else: raise new ValueError

func parseSecondInput(c: string) : Outcome =
  case c
  of "X": return Lose
  of "Y": return Draw
  of "Z": return Win
  else: raise new ValueError

func getMyMove(op: Hand, outcome: Outcome) : Hand =
  case outcome
  of Draw: return op
  of Win:
    case op
    of Rock: return Paper
    of Paper: return Scissor
    of Scissor: return Rock
  of Lose:
    case op:
      of Rock: return Scissor
      of Scissor: return Paper
      of Paper: return Rock


func winLoseTie(op, me: Hand) : int =
  case me
  of Rock:
    case op
    of Rock: return 3
    of Paper: return 0
    of Scissor: return 6
  of Paper:
    case op
    of Paper: return 3
    of Scissor: return 0
    of Rock: return 6
  of Scissor:
    case op
    of Scissor: return 3
    of Rock: return 0
    of Paper: return 6


proc scoreRound(op, me: Hand) : int =
  var shape: int
  case me
  of Rock: shape = 1
  of Paper: shape = 2
  of Scissor: shape = 3

  return shape + winLoseTie(op, me)

proc scoreRound(op: Hand, outcome: Outcome) : int =
  return scoreRound(op, getMyMove(op, outcome))


proc partOneScore() : int =
  let f = open("assets/day2.txt")
  defer: f.close()

  for l in f.lines:
    let chars = l.split()
    result += scoreRound(parseInput(chars[0]), parseInput(chars[^1]))


proc partTwoScore() : int =
  let f = open("assets/day2.txt")
  defer: f.close()

  for l in f.lines:
    let chars = l.split()
    result += scoreRound(parseInput(chars[0]), parseSecondInput(chars[^1]))




proc solve*() =
  echo "Day 2 Solution"
  echo "\tPart 1: " & $partOneScore()
  echo "\tPart 2: " & $partTwoScore()
