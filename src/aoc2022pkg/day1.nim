
import std/strutils
import math
import std/sequtils

proc findThreeMostCalories[M: static int]() : int = 
  let f = open("assets/day1.txt")
  defer: f.close()

  var topCalorieCounts: array[M, int]
  var currCalorieCount: int = 0

  for l in f.lines:
    if l.len == 0:
      let minCalIdx = minIndex(topCalorieCounts)
      if currCalorieCount > topCalorieCounts[minCalIdx]:
        topCalorieCounts[minCalIdx] = currCalorieCount
      currCalorieCount = 0
    else:
      currCalorieCount += l.parseInt
  return sum(topCalorieCounts)

proc solve*() = 
  echo "Day 1 Solution"
  ## Part 1
  echo "\tPart 1: ", findThreeMostCalories[1]()
  
  ## Part 2
  echo "\tPart 2: ", findThreeMostCalories[3]()



