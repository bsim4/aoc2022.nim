import std / sequtils

proc partSolutions[M: static int]() : int =
  let msg = open("./assets/day6.txt").readAll()
  var lastChars : array[M, char]
  for i in 0 ..< M:
    lastChars[i] = msg[i]

  result = M
  for i in M .. msg.len:
    if deduplicate(lastChars).len == M:
      return result
    lastChars[i mod M] = msg[i]
    result += 1
  raise new ValueError

proc solve*() = 
  echo "Day 6 Solution"
  echo "\tPart 1: ", partSolutions[4]()
  echo "\tPart 2: ", partSolutions[14]()
