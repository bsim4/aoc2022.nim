import std/sequtils
import std/strutils


type
  CrateIdx = 1..9

  CrateHolder = array[CrateIdx, seq[char]]

  PartVariant = enum
    One, Two

proc addItemsToCrate(viz: var seq[string], crates: var CrateHolder) = 
  let refLine = viz.pop
  var idxRef: array[CrateIdx, Natural] 
  for i in CrateIdx.low .. CrateIdx.high:
    idxRef[i] = refLine.find($i)
  while viz.len > 0:
    let currLine = viz.pop
    for i in CrateIdx.low .. CrateIdx.high:
      let c = currLine[idxRef[i]]
      if c != ' ':
        crates[i].add c



proc partSolutions(variant: PartVariant)  =
  let f = open("./assets/day5.txt")
  defer: f.close()

  var crates: CrateHolder
  var rawCrates: seq[string] = @[]

  var beforeMoves = true
  for l in f.lines:
    if l.len == 0:
      beforeMoves = false
      addItemsToCrate(rawCrates, crates)
      continue
    if beforeMoves:
      rawCrates.add l
    else:
      let ll = l.replace("move", "").replace("from", "").replace("to", "").replace("  ", " ").strip()
      let parts = ll.split(" ")
      var n = parts[0].parseInt
      let f = parts[1].parseInt.CrateIdx
      let t = parts[2].parseInt.CrateIdx
      case variant
      of One:
        while n > 0:
          crates[t].add crates[f].pop
          n -= 1
      of Two:
        var temp: seq[char] = @[]
        var nTemp = n
        while nTemp > 0:
          temp.add crates[f].pop
          nTemp -= 1
        while n > 0:
          crates[t].add temp.pop
          n -= 1

  for i in CrateIdx.low .. CrateIdx.high:
    echo crates[i].pop


proc solve*() =
  echo "Day 5 Solution"
  partSolutions(Two)
