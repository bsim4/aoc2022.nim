import std / strutils
import std / seqUtils
import std / sets

let testInput: string = 
  """
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
  """

type
  Pos = object
    x, y: int
  Dir = enum
    U, R, D, L

proc updatePos(dir: Dir, pos: var Pos) = 
  case dir
  of U: pos.y += 1
  of D: pos.y -= 1
  of R: pos.x += 1
  of L: pos.x -= 1

proc isTouching(pos1, pos2: Pos) : bool = 
  for dx in -1 .. 1:
    for dy in -1 .. 1:
      if Pos(x: pos1.x + dx, y: pos1.y + dy) == pos2:
        return true
  return false

proc updatePos(pos1: Pos, pos2: var Pos) =
  if isTouching(pos1, pos2):
    return
  if pos1.x == pos2.x:
    if pos1.y - pos2.y > 1:
      pos2.y += 1
    elif pos1.y - pos2.y < -1:
      pos2.y -= 1
  elif pos1.y == pos2.y:
    if pos1.x - pos2.x > 1:
      pos2.x += 1
    elif pos1.x - pos2.x < -1:
      pos2.x -= 1
  else:
    if pos1.x - pos2.x > 0:
      pos2.x += 1
    else:
      pos2.x -= 1

    if pos1.y - pos2.y > 0:
      pos2.y += 1
    else:
      pos2.y -= 1

    

proc parseDir(s: string) : Dir = 
  let c = s[0]
  case c
  of 'R': R
  of 'U': U
  of 'D': D
  of 'L': L
  else: raise new ValueError

proc plotPos(hPos: Pos, tailPositions: openArray[Pos], posSet: HashSet[Pos]) = 
  for y in -15..15:
    for x in -15..15:
      let currPos = Pos(x: x, y: y)
      if currPos == hPos:
        stdout.write "H"
      elif currPos in tailPositions:
        stdout.write $(find(tailPositions, currPos) + 1)
      elif posSet.contains(currPos):
        stdout.write "#"
      else:
        stdout.write "."
    stdout.write "\n"
  stdout.write "\n"
  stdout.flushFile()

proc solveInput(inText: string, numTails: static int = 2) : int =
  var tailPositions : array[numTails, Pos]

  var posSet = toHashSet(@[Pos()])
  
  for step in inText.strip().split("\n"):
    let parts = step.split(" ")
    let dir = parseDir(parts[0])
    let times = parts[^1].parseInt
    for _ in 1 .. times:
      updatePos(dir, tailPositions[0])
      for i in 0 ..< tailPositions.len - 1:
        updatePos(tailPositions[i], tailPositions[i + 1])
      posSet.incl(tailPositions[^1])
    # echo tailPositions
    # plotPos(tailPositions[0], tailPositions[1..^1], posSet)
  # echo posSet
  return posSet.len

let secondTestInput = """
R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
"""
proc solve*() = 
  echo "Day 9 Solutions"
  let dayInput = open("assets/day9.txt").readAll()
  # echo solveInput(testInput)
  echo "\tPart 1: ", solveInput(dayInput)
  echo "\tPart 2: ", solveInput(dayInput, 10)

