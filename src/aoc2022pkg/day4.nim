import std / strutils
type
  AssignRange = object
    l, h: int

proc parseAssignment(s: string) : AssignRange = 
  let parts = s.split("-")
  return AssignRange(l: parts[0].parseInt, h: parts[^1].parseInt)

proc fullyContains(ar1, ar2: AssignRange) : bool =
  return ar1.l >= ar2.l and ar1.h <= ar2.h

proc partlyOverlap(ar1, ar2: AssignRange) : bool =
  return ar1.l <= ar2.l and ar1.h >= ar2.l


proc partSolutions() : tuple[one, two: int] =

  let f = open("./assets/day4.txt")
  defer: f.close()

  var part1, part2: int
  for l in f.lines:
    let parts = l.split(",")
    let ar1 = parseAssignment(parts[0])
    let ar2 = parseAssignment(parts[^1])
    let fullyContained = fullyContains(ar1, ar2) or fullyContains(ar2, ar1)
    if fullyContained:
      part1 += 1
    let partlyOverlapped = partlyOverlap(ar1, ar2) or partlyOverlap(ar2, ar1)
    if partlyOverlapped:
      part2 += 1
  return (one: part1, two: part2)



proc solve*() =
  echo "Day 4 Solution"
  let (one, two) = partSolutions()
  echo "\tPart1: ", one
  echo "\tPart2: ", two
