# Package

version       = "0.1.0"
author        = "Bola Malek"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["aoc2022"]


# Dependencies

requires "nim >= 1.6.8"
requires "arraymancer"
